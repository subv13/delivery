# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-08 08:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20170608_0614'),
    ]

    operations = [
        migrations.CreateModel(
            name='SmsCode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(max_length=4, verbose_name='\u0421\u041c\u0421 \u043a\u043e\u0434')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='\u0421\u043e\u0437\u0434\u0430\u043d')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Client', verbose_name='\u041a\u043b\u0438\u0435\u043d\u0442')),
            ],
        ),
    ]
