# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from main.models import Client, SmsCode


class ClientAdmin(admin.ModelAdmin):
    pass


admin.site.register(Client, ClientAdmin)


@admin.register(SmsCode)
class SmsCodeAdmin(admin.ModelAdmin):
    pass
